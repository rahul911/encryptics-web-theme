import React from "react";
import {Col, Row} from "antd";
import TaskList from "components/dashboard/CRM/TaskList";
import RecentActivity from "components/dashboard/CRM/RecentActivity";
import TicketList from "components/dashboard/CRM/TicketList";
import TaskByStatus from "components/dashboard/CRM/TaskByStatus";
import Overview from "components/dashboard/CRM/Overview";
import SiteAudience from "components/dashboard/CRM/SiteAudience";
import TotalRevenueCard from "components/dashboard/CRM/TotalRevenueCard";
import NewCustomers from "components/dashboard/CRM/NewCustomers";
import GrowthCard from "components/dashboard/CRM/GrowthCard";
import Widget from "components/Widget/index";
import CurrencyCalculator from "components/dashboard/Crypto/CurrencyCalculator";
import IconWithTextCard from "components/dashboard/CRM/IconWithTextCard";
import {recentActivity, taskList, trafficData} from "./data";

const todaysTransactions = [
  { title: 'Authorized', value: '18', color: 'cyan' },
  { title: 'Unauthorized', value: '1', color: 'orange' },
  { title: 'Policy violations', value: '6', color: 'geekblue' },
];

const weeklyTransactions = [
  {name: 'Sun', Authorized: 200, Unauthorized: 600, PolicyViolations: 300},
  {name: 'Mon', Authorized: 200, Unauthorized: 600, PolicyViolations: 400},
  {name: 'Tue', Authorized: 200, Unauthorized: 600, PolicyViolations: 400},
  {name: 'Wed', Authorized: 200, Unauthorized: 600, PolicyViolations: 400},
  {name: 'Thu', Authorized: 200, Unauthorized: 600, PolicyViolations: 400},
  {name: 'Fri', Authorized: 200, Unauthorized: 600, PolicyViolations: 400},
  {name: 'Sat', Authorized: 200, Unauthorized: 600, PolicyViolations: 400},
];

const SamplePage = () => {
  return (
    <>
      <Row>
        <Col xl={4} lg={4} md={6} sm={24} xs={24}>
          <h2 className="title gx-mb-4">Welcome Jane</h2>
        </Col>
        <Col xl={20} lg={20} md={18} sm={24} xs={24}>
        </Col>

        <Col span={24}>
          <div className="gx-card">
            <div className="gx-card-body">
              <Row>
                <Col xl={6} lg={12} md={12} sm={24} xs={24}>
                  <SiteAudience titles={todaysTransactions} header={'Today\'s transactions'} />
                </Col>
                <Col xl={18} lg={12} md={12} sm={24} xs={24}>
                  <TotalRevenueCard total={7700} data={weeklyTransactions} />
                </Col>
              </Row>
            </div>
          </div>
        </Col>
        <Col span={18}>
          <Overview/>
        </Col>
        <Col span={6}>
          <TaskByStatus/>
        </Col>

        <Col span={18}>
          <Row>
            <Col span={8}>
              <IconWithTextCard cardColor="cyan" icon="important" title="21" subTitle="Active companies"/>
            </Col>
            <Col span={8}>
              <IconWithTextCard cardColor="red" icon="close-circle" title="1" subTitle="Expired companies"/>
            </Col>
            <Col span={8}>
              <IconWithTextCard cardColor="orange" icon="files" title="1" subTitle="Golden clients"/>
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};

export default SamplePage;
