import React from "react";
import {Route, Switch} from "react-router-dom";

import asyncComponent from "util/asyncComponent";

const App = ({match}) => (
  <div className="gx-main-content-wrapper">
    <Switch>
      <Route path={`${match.url}dashboard`} component={asyncComponent(() => import('./SamplePage'))}/>
    </Switch>
  </div>
);
/* <Route path={`${match.url}dashboard`} component={asyncComponent(() => import('./SamplePage'))}/> */

export default App;
