import React from "react";

import LineIndicator from "./LineIndicator";

const SiteAudience = (props) => {
  const { titles, header } = props;
  const totalValueThatMakesFullWidth = titles.map(t => Number(t.value)).reduce((a, b) => a + b, 0);
  return (
    <div className="gx-site-dash gx-mb-2 gx-pt-3 gx-pt-sm-0 gx-pt-xl-2">
      <h4 className="gx-text-uppercase gx-mb-2 gx-mb-sm-4">{header}</h4>
      <ul className="gx-line-indicator">
        {titles.map(({ title, value, color }, index) => {
          const percentWidth = Math.floor((value*100)/totalValueThatMakesFullWidth);
          console.log(percentWidth, totalValueThatMakesFullWidth);
          return (
            <li key={index}>
              <LineIndicator width={`${percentWidth}%`} title={title} color={color} value={value} />
            </li>
          );
        })}
      </ul>
    </div>
  )
};
export default SiteAudience;
