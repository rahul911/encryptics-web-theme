import React from "react";
import {Bar, BarChart, ResponsiveContainer, Legend, Tooltip, XAxis } from "recharts";
import {Col, Row} from "antd";

import Metrics from "components/Metrics";
import {connect} from "react-redux";

const TotalRevenueCard = ({width, data, total}) => {
  return (
    <>
      <Row>
        <Col xl={8} lg={8} md={8} sm={24} xs={24}>
        </Col>
        <Col xl={16} lg={16} md={16} sm={24} xs={24}>
          <div className="gx-wel-ema gx-pt-xl-2">
            <h4 className="gx-text-uppercase gx-mb-3">Weekly transactions</h4>
          </div>
        </Col>
        <br />
      </Row>
      <Row>
        <Col xl={8} lg={8} md={8} sm={24} xs={24}></Col>
        <Col xl={16} lg={16} md={16} sm={24} xs={24}>
          <h6 className="gx-text-uppercase gx-mb-2 gx-mb-sm-4">Total: {total}</h6>
        </Col>
        <br />
      </Row>
      <Row>
        <br />
      </Row>
      <Row>
        <Col xl={4} lg={4} md={4} sm={0} xs={0}>
        </Col>
        <Col xl={20} lg={20} md={20} sm={24} xs={24}>
          <ResponsiveContainer className="gx-barchart" width="100%" height={70}>
            <BarChart width={730} height={250} data={data} margin={{top: 0, right: 0, left: 0, bottom: 0}}>
              <XAxis dataKey="name" />
              <Tooltip/>
              <Legend />
              <Bar dataKey="Authorized" fill="cyan" barSize={width <= 575 ? 4 : 8}/>
              <Bar dataKey="Unauthorized" fill="orange" barSize={width <= 575 ? 4 : 8}/>
              <Bar dataKey="PolicyViolations" fill="geekblue" barSize={width <= 575 ? 4 : 8}/>
            </BarChart>
          </ResponsiveContainer>
        </Col>
      </Row>
    </>
  );
};


const mapStateToProps = ({settings}) => {
  const {width} = settings;
  return {width}
};
export default connect(mapStateToProps)(TotalRevenueCard);
